﻿using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace wikiscraper {
  public abstract class Generator {
    public abstract string Generate(string word, Dictionary<string[], string> map);

    public string MakeId(string word) {
      return word.Replace(' ', '-').Replace('\t', '-');
    }

    public string MakeClassName(string word) {
      var stringBuilder = new StringBuilder(word.Length);
      if (char.IsDigit(word[0])) {
        stringBuilder.Append("W");
      }

      bool nextCharToUpper = true;
      foreach (var c in word) {
        if (!char.IsLetterOrDigit(c)) {
          nextCharToUpper = true;
          continue;
        }

        var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
        if (unicodeCategory != UnicodeCategory.NonSpacingMark) {
          if (nextCharToUpper) {
            stringBuilder.Append(c.ToString().ToUpper());
            nextCharToUpper = false;
          } else {
            stringBuilder.Append(c);
          }
        } else {
          nextCharToUpper = true;
        }
      }

      return stringBuilder.ToString();
    }
  }
}