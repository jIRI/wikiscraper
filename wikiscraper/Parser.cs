﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace wikiscraper {
  public abstract class Parser {
    private Regex _wordRegex = new Regex(@"(\<a.*\>(?<word>[\w\s]*)\<\/a\>\s*\/\s*(\<a.*\>.*\<\/a\>\<\w.*\>.*\<\/\w\>))|(\<a.*\>(?<word>[\w\s]*)\<\/a\>\<sup.*\>.*\<\/sup\>\s*\/\s*.*)|(\<a.*\>(?<word>[\w\s]*)\<\/a\>\<sup.*\>.*\<\/sup\>)|(\<a.*\>(?<word>[\w\s]*)\<\/a\>\s*\/\s*.*)|(\<a.*\>(?<word>[\w\s]*)\<\/a\>)|((?<word>[\w\s]*)\s*\/\s*.*)|((?<word>[\w\s]*)\s+\(.*\))|(?<word>[\w\s]*)");

    public abstract (string word, Dictionary<string[], string> resultSet) Parse(string content);

    public void AddOrSet(Dictionary<string[], string> resultSet, string[] key, string value) {
      if (resultSet.ContainsKey(key)) {
        resultSet[key] = value;
      } else {
        resultSet.Add(key, value);
      }
    }

    public string GetWord(string tdContent) {
      var wordMatch = this._wordRegex.Match(tdContent);
      if (wordMatch.Success) {
        return wordMatch.Groups["word"].Value.Trim();
      } else {
        return tdContent.Trim();
      }
    }

  }
}