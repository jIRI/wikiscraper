﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Collections.Concurrent;

namespace wikiscraper {
  public class ItemDownloadedEventArgs : EventArgs {

    public string FilePath { get; private set; }

    public ItemDownloadedEventArgs(string filePath) {
      this.FilePath = filePath;
    }
  }

  public class Walker {
    private Regex _itemGroupStartRegex = new Regex(@"\<div class\=""mw-category-group""\>");
    private Regex _itemGroupEndRegex = new Regex(@"\<\/div\>");
    private Regex _itemLinkRegex = new Regex(@"\<a href\=""(?<link>.*)"" title\="".*""\>.*\<\/a\>");
    private Regex _nextPageRegex = new Regex(@"\)\W+\(\<a href\=""(?<link>.*)"" title\="".*""\>další stránka\<\/a\>");
    private string _itemsPath;
    private HttpClient _httpClient = new HttpClient();

    public event EventHandler<ItemDownloadedEventArgs> ItemReady;

    public Walker(string itemsPath) {
      _itemsPath = itemsPath;
    }

    public IEnumerable<string> DownloadAllPages(string url) {
      var page = this.DoDownloadAsync(url).Result;

      int itemPageCount = 0;
      var itemFilesList = new List<string>();
      var retryList = new List<(string file, string itemLink)>();
      while (true) {
        var (itemLinks, nextPageLink) = this.Parse(page);

        int itemCount = 0;
        foreach (var itemLink in itemLinks) {
          var itemFile = $"{this._itemsPath}\\{itemPageCount:D3}-{itemCount:D3}.html";
          try {
            var itemPageContent = this.DoDownloadAsync($"https://cs.wiktionary.org{itemLink}").Result;
            SaveFile(itemFile, itemPageContent);
            itemCount++;

            // for testing purposes stop after 3rd file
            //if (itemCount > 2) {
            //  break;
            //}
          } catch (Exception ex) {
            Console.WriteLine($"Error while downloading '{itemLink}': {ex}");
            retryList.Add((itemFile, itemLink));
          }
        }

        int retryCount = 3;
        while (retryCount-- > 0 && retryList.Count > 0) {
          var retryItem = retryList[0];
          retryList.RemoveAt(0);
          try {
            var itemPageContent = this.DoDownloadAsync($"https://cs.wiktionary.org{retryItem.itemLink}").Result;
            SaveFile(retryItem.file, itemPageContent);
          } catch {
            retryList.Add(retryItem);
          }
        }
        //if (itemPageCount > 2) {
        //  break;
        //}

        if (string.IsNullOrEmpty(nextPageLink)) {
          break;
        } else {
          page = this.DoDownloadAsync($"https://cs.wiktionary.org{nextPageLink}").Result;
        }
        itemPageCount++;
      }

      return itemFilesList;

      void SaveFile(string itemFile, string itemPageContent) {
        using (var file = File.CreateText(itemFile)) {
          file.Write(itemPageContent);
          itemFilesList.Add(itemFile);
        }
        this.ItemReady?.Invoke(this, new ItemDownloadedEventArgs(itemFile));
      }
    }

    private async Task<string> DoDownloadAsync(string url) {
      return await this._httpClient.GetStringAsync(url);
    }

    public (IEnumerable<string> itemLinks, string nextPageLink) Parse(string pageContent) {
      var itemLinks = new List<string>();
      var nextSearchStartPos = 0;
      while (nextSearchStartPos < pageContent.Length) {
        var sectionStartMatch = this._itemGroupStartRegex.Match(pageContent, nextSearchStartPos);
        if (sectionStartMatch.Success) {
          var sectionStartPos = sectionStartMatch.Index + sectionStartMatch.Length;
          var sectionEndMatch = this._itemGroupEndRegex.Match(pageContent, sectionStartPos);
          var sectionContent = pageContent.Substring(sectionStartPos, sectionEndMatch.Index - sectionStartPos);
          nextSearchStartPos = sectionEndMatch.Index + sectionEndMatch.Length;
          this.ParseitemLinks(sectionContent, itemLinks);
        } else {
          break;
        }
      }

      return (itemLinks, this.ParseNextPageLink(pageContent));
    }

    public void ParseitemLinks(string sectionContent, IList<string> itemLinks) {
      var itemLinksMatches = this._itemLinkRegex.Matches(sectionContent);
      foreach (Match match in itemLinksMatches) {
        var link = match.Groups["link"].Value;
        itemLinks.Add(link);
      }
    }

    public string ParseNextPageLink(string sectionContent) {
      var nextPageLinkMatch = this._nextPageRegex.Match(sectionContent);
      if (nextPageLinkMatch.Success) {
        return nextPageLinkMatch.Groups["link"].Value.Replace("&amp;", "&");
      } else {
        return null;
      }
    }
  }
}
