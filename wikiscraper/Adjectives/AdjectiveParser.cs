﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace wikiscraper {
  public class AdjectiveParser : Parser {
    private Regex _adjectiveRegex = new Regex(@"\<h1 id\=""firstHeading"" class\=""firstHeading"" lang\=""cs""\>(?<noun>.*)\<\/h1\>");
    private Regex _tableStartRegex = new Regex(@"\<table\s+class=\""deklinace adjektivum\""\>");
    private Regex _tableEndRegex = new Regex(@"\<\/table\>");
    private Regex _trRegex = new Regex(@"\<tr\>");
    private Regex _tdRegex = new Regex(@"\<td\>(?<td>.*)\s*\<\/td\>");
    private Dictionary<string, string[]> _nounMap = new Dictionary<string, string[]> {
      ["singular.masculine.nominative"] = new string[] { "singular", "masculine", "nominative" },
      ["singular.masculine.genitive"] = new string[] { "singular", "masculine", "genitive" },
      ["singular.masculine.dative"] = new string[] { "singular", "masculine", "dative" },
      ["singular.masculine.accusative"] = new string[] { "singular", "masculine", "accusative" },
      ["singular.masculine.vocative"] = new string[] { "singular", "masculine", "vocative" },
      ["singular.masculine.locative"] = new string[] { "singular", "masculine", "locative" },
      ["singular.masculine.instrumental"] = new string[] { "singular", "masculine", "instrumental" },

      ["singular.masculine.nominative.animate"] = new string[] { "singular", "masculine", "nominative", "animate" },
      ["singular.masculine.genitive.animate"] = new string[] { "singular", "masculine", "genitive", "animate" },
      ["singular.masculine.dative.animate"] = new string[] { "singular", "masculine", "dative", "animate" },
      ["singular.masculine.accusative.animate"] = new string[] { "singular", "masculine", "accusative", "animate" },
      ["singular.masculine.vocative.animate"] = new string[] { "singular", "masculine", "vocative", "animate" },
      ["singular.masculine.locative.animate"] = new string[] { "singular", "masculine", "locative", "animate" },
      ["singular.masculine.instrumental.animate"] = new string[] { "singular", "masculine", "instrumental", "animate" },

      ["singular.masculine.nominative.inanimate"] = new string[] { "singular", "masculine", "nominative", "inanimate" },
      ["singular.masculine.genitive.inanimate"] = new string[] { "singular", "masculine", "genitive", "inanimate" },
      ["singular.masculine.dative.inanimate"] = new string[] { "singular", "masculine", "dative", "inanimate" },
      ["singular.masculine.accusative.inanimate"] = new string[] { "singular", "masculine", "accusative", "inanimate" },
      ["singular.masculine.vocative.inanimate"] = new string[] { "singular", "masculine", "vocative", "inanimate" },
      ["singular.masculine.locative.inanimate"] = new string[] { "singular", "masculine", "locative", "inanimate" },
      ["singular.masculine.instrumental.inanimate"] = new string[] { "singular", "masculine", "instrumental", "inanimate" },

      ["singular.feminine.nominative"] = new string[] { "singular", "feminine", "nominative" },
      ["singular.feminine.genitive"] = new string[] { "singular", "feminine", "genitive" },
      ["singular.feminine.dative"] = new string[] { "singular", "feminine", "dative" },
      ["singular.feminine.accusative"] = new string[] { "singular", "feminine", "accusative" },
      ["singular.feminine.vocative"] = new string[] { "singular", "feminine", "vocative" },
      ["singular.feminine.locative"] = new string[] { "singular", "feminine", "locative" },
      ["singular.feminine.instrumental"] = new string[] { "singular", "feminine", "instrumental" },

      ["singular.neutral.nominative"] = new string[] { "singular", "neutral", "nominative" },
      ["singular.neutral.genitive"] = new string[] { "singular", "neutral", "genitive" },
      ["singular.neutral.dative"] = new string[] { "singular", "neutral", "dative" },
      ["singular.neutral.accusative"] = new string[] { "singular", "neutral", "accusative" },
      ["singular.neutral.vocative"] = new string[] { "singular", "neutral", "vocative" },
      ["singular.neutral.locative"] = new string[] { "singular", "neutral", "locative" },
      ["singular.neutral.instrumental"] = new string[] { "singular", "neutral", "instrumental" },

      ["plural.masculine.nominative"] = new string[] { "plural", "masculine", "nominative" },
      ["plural.masculine.genitive"] = new string[] { "plural", "masculine", "genitive" },
      ["plural.masculine.dative"] = new string[] { "plural", "masculine", "dative" },
      ["plural.masculine.accusative"] = new string[] { "plural", "masculine", "accusative" },
      ["plural.masculine.vocative"] = new string[] { "plural", "masculine", "vocative" },
      ["plural.masculine.locative"] = new string[] { "plural", "masculine", "locative" },
      ["plural.masculine.instrumental"] = new string[] { "plural", "masculine", "instrumental" },

      ["plural.masculine.nominative.animate"] = new string[] { "plural", "masculine", "nominative", "animate" },
      ["plural.masculine.genitive.animate"] = new string[] { "plural", "masculine", "genitive", "animate" },
      ["plural.masculine.dative.animate"] = new string[] { "plural", "masculine", "dative", "animate" },
      ["plural.masculine.accusative.animate"] = new string[] { "plural", "masculine", "accusative", "animate" },
      ["plural.masculine.vocative.animate"] = new string[] { "plural", "masculine", "vocative", "animate" },
      ["plural.masculine.locative.animate"] = new string[] { "plural", "masculine", "locative", "animate" },
      ["plural.masculine.instrumental.animate"] = new string[] { "plural", "masculine", "instrumental", "animate" },

      ["plural.masculine.nominative.inanimate"] = new string[] { "plural", "masculine", "nominative", "inanimate" },
      ["plural.masculine.genitive.inanimate"] = new string[] { "plural", "masculine", "genitive", "inanimate" },
      ["plural.masculine.dative.inanimate"] = new string[] { "plural", "masculine", "dative", "inanimate" },
      ["plural.masculine.accusative.inanimate"] = new string[] { "plural", "masculine", "accusative", "inanimate" },
      ["plural.masculine.vocative.inanimate"] = new string[] { "plural", "masculine", "vocative", "inanimate" },
      ["plural.masculine.locative.inanimate"] = new string[] { "plural", "masculine", "locative", "inanimate" },
      ["plural.masculine.instrumental.inanimate"] = new string[] { "plural", "masculine", "instrumental", "inanimate" },

      ["plural.feminine.nominative"] = new string[] { "plural", "feminine", "nominative" },
      ["plural.feminine.genitive"] = new string[] { "plural", "feminine", "genitive" },
      ["plural.feminine.dative"] = new string[] { "plural", "feminine", "dative" },
      ["plural.feminine.accusative"] = new string[] { "plural", "feminine", "accusative" },
      ["plural.feminine.vocative"] = new string[] { "plural", "feminine", "vocative" },
      ["plural.feminine.locative"] = new string[] { "plural", "feminine", "locative" },
      ["plural.feminine.instrumental"] = new string[] { "plural", "feminine", "instrumental" },

      ["plural.neutral.nominative"] = new string[] { "plural", "neutral", "nominative" },
      ["plural.neutral.genitive"] = new string[] { "plural", "neutral", "genitive" },
      ["plural.neutral.dative"] = new string[] { "plural", "neutral", "dative" },
      ["plural.neutral.accusative"] = new string[] { "plural", "neutral", "accusative" },
      ["plural.neutral.vocative"] = new string[] { "plural", "neutral", "vocative" },
      ["plural.neutral.locative"] = new string[] { "plural", "neutral", "locative" },
      ["plural.neutral.instrumental"] = new string[] { "plural", "neutral", "instrumental" },
    };

    public override (string word, Dictionary<string[], string> resultSet) Parse(string pageContent) {
      var resultSet = new Dictionary<string[], string>();
      var nounMatch = this._adjectiveRegex.Match(pageContent);
      string noun = nounMatch.Groups["noun"].Value;
      var tableStartMatches = this._tableStartRegex.Matches(pageContent);
      foreach (Match match in tableStartMatches) {
        int startPos = match.Index + match.Length;
        var tableEndMatch = this._tableEndRegex.Match(pageContent, startPos);
        var tableContent = pageContent.Substring(startPos, tableEndMatch.Index - startPos);

        if (!string.IsNullOrWhiteSpace(tableContent)) {
          this.ParseAdjective(tableContent, resultSet);
        }
        break;
      }

      if (resultSet.Count == 0 || resultSet.Values.Any(v => string.IsNullOrEmpty(v))) {
        return (null, null);
      }

      return (noun, resultSet);
    }

    private void ParseAdjective(string tableContent, Dictionary<string[], string> resultSet) {
      var caseMatch = this._trRegex.Match(tableContent);
      if (caseMatch.Success) {
        var match = this._tdRegex.Match(tableContent, caseMatch.Index);
        var masculineAnimate = this.GetWord(match.Groups["td"].Value);
        match = match.NextMatch();
        var masculineInanimate = this.GetWord(match.Groups["td"].Value);
        match = match.NextMatch();
        AddOrSetMaculine(resultSet, "singular.masculine.nominative", masculineAnimate, masculineInanimate);
        this.AddOrSet(resultSet, this._nounMap["singular.feminine.nominative"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._nounMap["singular.neutral.nominative"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        masculineAnimate = this.GetWord(match.Groups["td"].Value);
        match = match.NextMatch();
        masculineInanimate = this.GetWord(match.Groups["td"].Value);
        match = match.NextMatch();
        AddOrSetMaculine(resultSet, "plural.masculine.nominative", masculineAnimate, masculineInanimate);
        this.AddOrSet(resultSet, this._nounMap["plural.feminine.nominative"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._nounMap["plural.neutral.nominative"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();

        match = this._tdRegex.Match(tableContent, match.Index);
        masculineAnimate = this.GetWord(match.Groups["td"].Value);
        match = match.NextMatch();
        masculineInanimate = this.GetWord(match.Groups["td"].Value);
        match = match.NextMatch();
        AddOrSetMaculine(resultSet, "singular.masculine.genitive", masculineAnimate, masculineInanimate);
        this.AddOrSet(resultSet, this._nounMap["singular.feminine.genitive"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._nounMap["singular.neutral.genitive"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        masculineAnimate = this.GetWord(match.Groups["td"].Value);
        match = match.NextMatch();
        masculineInanimate = this.GetWord(match.Groups["td"].Value);
        match = match.NextMatch();
        AddOrSetMaculine(resultSet, "plural.masculine.genitive", masculineAnimate, masculineInanimate);
        this.AddOrSet(resultSet, this._nounMap["plural.feminine.genitive"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._nounMap["plural.neutral.genitive"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();

        match = this._tdRegex.Match(tableContent, match.Index);
        masculineAnimate = this.GetWord(match.Groups["td"].Value);
        match = match.NextMatch();
        masculineInanimate = this.GetWord(match.Groups["td"].Value);
        match = match.NextMatch();
        AddOrSetMaculine(resultSet, "singular.masculine.dative", masculineAnimate, masculineInanimate);
        this.AddOrSet(resultSet, this._nounMap["singular.feminine.dative"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._nounMap["singular.neutral.dative"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        masculineAnimate = this.GetWord(match.Groups["td"].Value);
        match = match.NextMatch();
        masculineInanimate = this.GetWord(match.Groups["td"].Value);
        match = match.NextMatch();
        AddOrSetMaculine(resultSet, "plural.masculine.dative", masculineAnimate, masculineInanimate);
        this.AddOrSet(resultSet, this._nounMap["plural.feminine.dative"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._nounMap["plural.neutral.dative"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();

        match = this._tdRegex.Match(tableContent, match.Index);
        masculineAnimate = this.GetWord(match.Groups["td"].Value);
        match = match.NextMatch();
        masculineInanimate = this.GetWord(match.Groups["td"].Value);
        match = match.NextMatch();
        AddOrSetMaculine(resultSet, "singular.masculine.accusative", masculineAnimate, masculineInanimate);
        this.AddOrSet(resultSet, this._nounMap["singular.feminine.accusative"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._nounMap["singular.neutral.accusative"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        masculineAnimate = this.GetWord(match.Groups["td"].Value);
        match = match.NextMatch();
        masculineInanimate = this.GetWord(match.Groups["td"].Value);
        match = match.NextMatch();
        AddOrSetMaculine(resultSet, "plural.masculine.accusative", masculineAnimate, masculineInanimate);
        this.AddOrSet(resultSet, this._nounMap["plural.feminine.accusative"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._nounMap["plural.neutral.accusative"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();

        match = this._tdRegex.Match(tableContent, match.Index);
        masculineAnimate = this.GetWord(match.Groups["td"].Value);
        match = match.NextMatch();
        masculineInanimate = this.GetWord(match.Groups["td"].Value);
        match = match.NextMatch();
        AddOrSetMaculine(resultSet, "singular.masculine.vocative", masculineAnimate, masculineInanimate);
        this.AddOrSet(resultSet, this._nounMap["singular.feminine.vocative"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._nounMap["singular.neutral.vocative"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        masculineAnimate = this.GetWord(match.Groups["td"].Value);
        match = match.NextMatch();
        masculineInanimate = this.GetWord(match.Groups["td"].Value);
        match = match.NextMatch();
        AddOrSetMaculine(resultSet, "plural.masculine.vocative", masculineAnimate, masculineInanimate);
        this.AddOrSet(resultSet, this._nounMap["plural.feminine.vocative"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._nounMap["plural.neutral.vocative"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();

        match = this._tdRegex.Match(tableContent, match.Index);
        masculineAnimate = this.GetWord(match.Groups["td"].Value);
        match = match.NextMatch();
        masculineInanimate = this.GetWord(match.Groups["td"].Value);
        match = match.NextMatch();
        AddOrSetMaculine(resultSet, "singular.masculine.locative", masculineAnimate, masculineInanimate);
        this.AddOrSet(resultSet, this._nounMap["singular.feminine.locative"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._nounMap["singular.neutral.locative"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        masculineAnimate = this.GetWord(match.Groups["td"].Value);
        match = match.NextMatch();
        masculineInanimate = this.GetWord(match.Groups["td"].Value);
        match = match.NextMatch();
        AddOrSetMaculine(resultSet, "plural.masculine.locative", masculineAnimate, masculineInanimate);
        this.AddOrSet(resultSet, this._nounMap["plural.feminine.locative"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._nounMap["plural.neutral.locative"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();

        match = this._tdRegex.Match(tableContent, match.Index);
        masculineAnimate = this.GetWord(match.Groups["td"].Value);
        match = match.NextMatch();
        masculineInanimate = this.GetWord(match.Groups["td"].Value);
        match = match.NextMatch();
        AddOrSetMaculine(resultSet, "singular.masculine.instrumental", masculineAnimate, masculineInanimate);
        this.AddOrSet(resultSet, this._nounMap["singular.feminine.instrumental"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._nounMap["singular.neutral.instrumental"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        masculineAnimate = this.GetWord(match.Groups["td"].Value);
        match = match.NextMatch();
        masculineInanimate = this.GetWord(match.Groups["td"].Value);
        match = match.NextMatch();
        AddOrSetMaculine(resultSet, "plural.masculine.instrumental", masculineAnimate, masculineInanimate);
        this.AddOrSet(resultSet, this._nounMap["plural.feminine.instrumental"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._nounMap["plural.neutral.instrumental"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
      }
    }

    private void AddOrSetMaculine(Dictionary<string[], string> resultSet, string mapId, string masculineAnimate, string masculineInanimate) {
      if (masculineAnimate != masculineInanimate) {
        this.AddOrSet(resultSet, this._nounMap[$"{mapId}.animate"], masculineAnimate);
        this.AddOrSet(resultSet, this._nounMap[$"{mapId}.inanimate"], masculineInanimate);
      } else {
        this.AddOrSet(resultSet, this._nounMap[mapId], masculineAnimate);
      }
    }
  }
}