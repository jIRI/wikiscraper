﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wikiscraper {
  public class AdjectiveGenerator : Generator {
    public override string Generate(string adjective, Dictionary<string[], string> adjectiveMap) {

      var verbDef = new StringBuilder(1024);
      var verbId = this.MakeId(adjective);
      verbDef.AppendFormat("export class {0} extends lang.Adjective {{\n", this.MakeClassName(adjective));
      verbDef.AppendFormat("  public static readonly ID = `${{lang.Adjective.ID}}.{0}`;\n", verbId.Trim().ToLowerInvariant());
      verbDef.Append("  constructor() {\n");
      verbDef.Append("    super({\n");
      verbDef.AppendFormat("      adjective: '{0}',\n", adjective.Trim().ToLowerInvariant());

      var level1 = from v in adjectiveMap.Keys
                     group v by v[0] into vv
                     select vv;
                     
      foreach(var level1Group in level1) {
        var level2 = from v in level1Group
                     group v by v[1] into vv
                     select vv;
        verbDef.AppendFormat("      {0}: {{\n", level1Group.Key);
        foreach (var level2Group in level2) {
          if(level2Group.Count() == 1) {
            verbDef.AppendFormat("        {0}: '{1}',\n", level2Group.Key, adjectiveMap[level2Group.First()].Trim());
          } else {
            verbDef.AppendFormat("        {0}: {{\n", level2Group.Key);
            var level3 = from v in level2Group
                          group v by v[2] into vv
                          select vv;
            foreach (var level3Group in level3) {
              if (level3Group.Count() == 1) {
                verbDef.AppendFormat("          {0}: '{1}',\n", level3Group.Key, adjectiveMap[level3Group.First()].Trim());
              } else {
                verbDef.AppendFormat("          {0}: {{", level3Group.Key);
                foreach (var level4 in level3Group) {
                  verbDef.AppendFormat(" {0}: '{1}',", level4.Last(), adjectiveMap[level4].Trim());
                }
                verbDef.Append(" },\n");
              }
            }
            verbDef.Append("        },\n");
          }
        }
        verbDef.Append("      },\n");
      }
      verbDef.Append("    });\n");
      verbDef.Append("  }\n");
      verbDef.Append("}\n\n");
      return verbDef.ToString();
    }
  }
}
