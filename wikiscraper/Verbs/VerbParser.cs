﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace wikiscraper {
  public class VerbParser : Parser {
    private Regex _verbRegex = new Regex(@"\<h1 id\=""firstHeading"" class\=""firstHeading"" lang\=""cs""\>(?<verb>.*)\<\/h1\>");
    private Regex _tableStartRegex = new Regex(@"\<table\s+class=\""konjugace verbum\""\>");
    private Regex _tableEndRegex = new Regex(@"\<\/table\>");
    private Regex _normalFormRegex = new Regex(@"\<caption\>\s*Oznamovací způsob\s*\<\/caption\>");
    private Regex _presentTenseRegex = new Regex(@"\<th\>\s*Přítomný čas\s*\<\/th\>");
    private Regex _futureTenseRegex = new Regex(@"\<th\>\s*Budoucí čas\s*\<\/th\>");
    private Regex _imperativeRegex = new Regex(@"\<caption\>\s*Rozkazovací způsob\s*\<\/caption\>");
    private Regex _otherFormsRegex = new Regex(@"\<caption\>\s*Příčestí\s*\<\/caption\>");
    private Regex _pastTenseRegex = new Regex(@"\<th\>\s*Činné\s*\<\/th\>");
    private Regex _passiveRegex = new Regex(@"\<th\>\s*Trpné\s*\<\/th\>");
    private Regex _participleFormRegex = new Regex(@"\<caption\>\s*Přechodníky\s*\<\/caption\>");
    private Regex _tdRegex = new Regex(@"\<td\>(?<td>.*)\s*\<\/td\>");
    private Regex _trRegex = new Regex(@"\<tr\>");
    private Dictionary<string, string[]> _formMap = new Dictionary<string, string[]> {
      ["singular.participle.masculine"] = new string[] { "singular", "participle", "masculine" },
      ["singular.participle.feminine"] = new string[] { "singular", "participle", "feminine" },
      ["singular.participle.neutral"] = new string[] { "singular", "participle", "neutral" },
      ["plural.participle"] = new string[] { "plural", "participle" },
      ["singular.past.first.masculine"] = new string[] { "singular", "past", "first", "masculine" },
      ["singular.past.second.masculine"] = new string[] { "singular", "past", "second", "masculine" },
      ["singular.past.third.masculine"] = new string[] { "singular", "past", "third", "masculine" },
      ["singular.past.first.feminine"] = new string[] { "singular", "past", "first", "feminine" },
      ["singular.past.second.feminine"] = new string[] { "singular", "past", "second", "feminine" },
      ["singular.past.third.feminine"] = new string[] { "singular", "past", "third", "feminine" },
      ["singular.past.first.neutral"] = new string[] { "singular", "past", "first", "neutral" },
      ["singular.past.second.neutral"] = new string[] { "singular", "past", "second", "neutral" },
      ["singular.past.third.neutral"] = new string[] { "singular", "past", "third", "neutral" },
      ["plural.past.first.masculine"] = new string[] { "plural", "past", "first", "masculine" },
      ["plural.past.second.masculine"] = new string[] { "plural", "past", "second", "masculine" },
      ["plural.past.third.masculine"] = new string[] { "plural", "past", "third", "masculine" },
      ["plural.past.first.feminine"] = new string[] { "plural", "past", "first", "feminine" },
      ["plural.past.second.feminine"] = new string[] { "plural", "past", "second", "feminine" },
      ["plural.past.third.feminine"] = new string[] { "plural", "past", "third", "feminine" },
      ["plural.past.first.neutral"] = new string[] { "plural", "past", "first", "neutral" },
      ["plural.past.second.neutral"] = new string[] { "plural", "past", "second", "neutral" },
      ["plural.past.third.neutral"] = new string[] { "plural", "past", "third", "neutral" },
      ["singular.passive.first.masculine"] = new string[] { "singular", "passive", "first", "masculine" },
      ["singular.passive.second.masculine"] = new string[] { "singular", "passive", "second", "masculine" },
      ["singular.passive.third.masculine"] = new string[] { "singular", "passive", "third", "masculine" },
      ["singular.passive.first.feminine"] = new string[] { "singular", "passive", "first", "feminine" },
      ["singular.passive.second.feminine"] = new string[] { "singular", "passive", "second", "feminine" },
      ["singular.passive.third.feminine"] = new string[] { "singular", "passive", "third", "feminine" },
      ["singular.passive.first.neutral"] = new string[] { "singular", "passive", "first", "neutral" },
      ["singular.passive.second.neutral"] = new string[] { "singular", "passive", "second", "neutral" },
      ["singular.passive.third.neutral"] = new string[] { "singular", "passive", "third", "neutral" },
      ["plural.passive.first.masculine"] = new string[] { "plural", "passive", "first", "masculine" },
      ["plural.passive.second.masculine"] = new string[] { "plural", "passive", "second", "masculine" },
      ["plural.passive.third.masculine"] = new string[] { "plural", "passive", "third", "masculine" },
      ["plural.passive.first.feminine"] = new string[] { "plural", "passive", "first", "feminine" },
      ["plural.passive.second.feminine"] = new string[] { "plural", "passive", "second", "feminine" },
      ["plural.passive.third.feminine"] = new string[] { "plural", "passive", "third", "feminine" },
      ["plural.passive.first.neutral"] = new string[] { "plural", "passive", "first", "neutral" },
      ["plural.passive.second.neutral"] = new string[] { "plural", "passive", "second", "neutral" },
      ["plural.passive.third.neutral"] = new string[] { "plural", "passive", "third", "neutral" },
      ["singular.present.first"] = new string[] { "singular", "present", "first" },
      ["singular.present.second"] = new string[] { "singular", "present", "second" },
      ["singular.present.third"] = new string[] { "singular", "present", "third" },
      ["plural.present.first"] = new string[] { "plural", "present", "first" },
      ["plural.present.second"] = new string[] { "plural", "present", "second" },
      ["plural.present.third"] = new string[] { "plural", "present", "third" },
      ["singular.future.first"] = new string[] { "singular", "future", "first" },
      ["singular.future.second"] = new string[] { "singular", "future", "second" },
      ["singular.future.third"] = new string[] { "singular", "future", "third" },
      ["plural.future.first"] = new string[] { "plural", "future", "first" },
      ["plural.future.second"] = new string[] { "plural", "future", "second" },
      ["plural.future.third"] = new string[] { "plural", "future", "third" },
      ["singular.imperative.second"] = new string[] { "singular", "imperative", "second" },
      ["plural.imperative.second"] = new string[] { "plural", "imperative", "second" },
      ["plural.imperative.third"] = new string[] { "plural", "imperative", "third" },
    };

    public override (string word, Dictionary<string[], string> resultSet) Parse(string pageContent) {
      var resultSet = new Dictionary<string[], string>();
      var verbMatch = this._verbRegex.Match(pageContent);
      string verb = verbMatch.Groups["verb"].Value;
      var tableStartMatches = this._tableStartRegex.Matches(pageContent);
      foreach (Match match in tableStartMatches) {
        int startPos = match.Index + match.Length;
        var tableEndMatch = this._tableEndRegex.Match(pageContent, startPos);
        var tableContent = pageContent.Substring(startPos, tableEndMatch.Index - startPos);
        var captionMatch = this._normalFormRegex.Match(tableContent);

        if (captionMatch.Success) {
          this.ParseNormalForm(tableContent, resultSet, verb);
          continue;
        }
        captionMatch = this._imperativeRegex.Match(tableContent);
        if (captionMatch.Success) {
          this.ParseImperativeForm(tableContent, resultSet);
          continue;
        }
        captionMatch = this._otherFormsRegex.Match(tableContent);
        if (captionMatch.Success) {
          this.ParseOtherForm(tableContent, resultSet);
          continue;
        }
        captionMatch = this._participleFormRegex.Match(tableContent);
        if (captionMatch.Success) {
          this.ParticipleForm(tableContent, resultSet);
          continue;
        }
      }

      return (verb, resultSet);
    }

    private void ParticipleForm(string tableContent, Dictionary<string[], string> resultSet) {
      var participleMatch = this._participleFormRegex.Match(tableContent);
      if (participleMatch.Success) {
        var match = this._tdRegex.Match(tableContent, participleMatch.Index);
        this.AddOrSet(resultSet, this._formMap["singular.participle.masculine"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._formMap["singular.participle.feminine"], this.GetWord(match.Groups["td"].Value));
        this.AddOrSet(resultSet, this._formMap["singular.participle.neutral"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._formMap["plural.participle"], this.GetWord(match.Groups["td"].Value));
      }
    }

    private void ParseOtherForm(string tableContent, Dictionary<string[], string> resultSet) {
      var pastTenseMatch = this._pastTenseRegex.Match(tableContent);
      if (pastTenseMatch.Success) {
        var match = this._tdRegex.Match(tableContent, pastTenseMatch.Index);
        this.AddOrSet(resultSet, this._formMap["singular.past.first.masculine"], this.GetWord(match.Groups["td"].Value));
        this.AddOrSet(resultSet, this._formMap["singular.past.second.masculine"], this.GetWord(match.Groups["td"].Value));
        this.AddOrSet(resultSet, this._formMap["singular.past.third.masculine"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._formMap["singular.past.first.feminine"], this.GetWord(match.Groups["td"].Value));
        this.AddOrSet(resultSet, this._formMap["singular.past.second.feminine"], this.GetWord(match.Groups["td"].Value));
        this.AddOrSet(resultSet, this._formMap["singular.past.third.feminine"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._formMap["singular.past.first.neutral"], this.GetWord(match.Groups["td"].Value));
        this.AddOrSet(resultSet, this._formMap["singular.past.second.neutral"], this.GetWord(match.Groups["td"].Value));
        this.AddOrSet(resultSet, this._formMap["singular.past.third.neutral"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._formMap["plural.past.first.masculine"], this.GetWord(match.Groups["td"].Value));
        this.AddOrSet(resultSet, this._formMap["plural.past.second.masculine"], this.GetWord(match.Groups["td"].Value));
        this.AddOrSet(resultSet, this._formMap["plural.past.third.masculine"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._formMap["plural.past.first.feminine"], this.GetWord(match.Groups["td"].Value));
        this.AddOrSet(resultSet, this._formMap["plural.past.second.feminine"], this.GetWord(match.Groups["td"].Value));
        this.AddOrSet(resultSet, this._formMap["plural.past.third.feminine"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._formMap["plural.past.first.neutral"], this.GetWord(match.Groups["td"].Value));
        this.AddOrSet(resultSet, this._formMap["plural.past.second.neutral"], this.GetWord(match.Groups["td"].Value));
        this.AddOrSet(resultSet, this._formMap["plural.past.third.neutral"], this.GetWord(match.Groups["td"].Value));
      }

      var passiveMatch = this._passiveRegex.Match(tableContent);
      if (passiveMatch.Success) {
        var match = this._tdRegex.Match(tableContent, passiveMatch.Index);
        this.AddOrSet(resultSet, this._formMap["singular.passive.first.masculine"], this.GetWord(match.Groups["td"].Value));
        this.AddOrSet(resultSet, this._formMap["singular.passive.second.masculine"], this.GetWord(match.Groups["td"].Value));
        this.AddOrSet(resultSet, this._formMap["singular.passive.third.masculine"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._formMap["singular.passive.first.feminine"], this.GetWord(match.Groups["td"].Value));
        this.AddOrSet(resultSet, this._formMap["singular.passive.second.feminine"], this.GetWord(match.Groups["td"].Value));
        this.AddOrSet(resultSet, this._formMap["singular.passive.third.feminine"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._formMap["singular.passive.first.neutral"], this.GetWord(match.Groups["td"].Value));
        this.AddOrSet(resultSet, this._formMap["singular.passive.second.neutral"], this.GetWord(match.Groups["td"].Value));
        this.AddOrSet(resultSet, this._formMap["singular.passive.third.neutral"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._formMap["plural.passive.first.masculine"], this.GetWord(match.Groups["td"].Value));
        this.AddOrSet(resultSet, this._formMap["plural.passive.second.masculine"], this.GetWord(match.Groups["td"].Value));
        this.AddOrSet(resultSet, this._formMap["plural.passive.third.masculine"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._formMap["plural.passive.first.feminine"], this.GetWord(match.Groups["td"].Value));
        this.AddOrSet(resultSet, this._formMap["plural.passive.second.feminine"], this.GetWord(match.Groups["td"].Value));
        this.AddOrSet(resultSet, this._formMap["plural.passive.third.feminine"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._formMap["plural.passive.first.neutral"], this.GetWord(match.Groups["td"].Value));
        this.AddOrSet(resultSet, this._formMap["plural.passive.second.neutral"], this.GetWord(match.Groups["td"].Value));
        this.AddOrSet(resultSet, this._formMap["plural.passive.third.neutral"], this.GetWord(match.Groups["td"].Value));
      }

    }

    private void ParseImperativeForm(string tableContent, Dictionary<string[], string> resultSet) {
      var imperativeMatch = this._imperativeRegex.Match(tableContent);
      if (imperativeMatch.Success) {
        var trMatch = this._trRegex.Match(tableContent).NextMatch().NextMatch();

        var match = this._tdRegex.Match(tableContent, trMatch.Index);
        this.AddOrSet(resultSet, this._formMap["singular.imperative.second"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._formMap["plural.imperative.second"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._formMap["plural.imperative.third"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
      }
    }

    private void ParseNormalForm(string tableContent, Dictionary<string[], string> resultSet, string infinitive) {
      var presentTenseMatch = this._presentTenseRegex.Match(tableContent);
      if (presentTenseMatch.Success) {
        var match = this._tdRegex.Match(tableContent, presentTenseMatch.Index);
        this.AddOrSet(resultSet, this._formMap["singular.present.first"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._formMap["singular.present.second"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._formMap["singular.present.third"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._formMap["plural.present.first"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._formMap["plural.present.second"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._formMap["plural.present.third"], this.GetWord(match.Groups["td"].Value));
      }

      var futureTenseMatch = this._futureTenseRegex.Match(tableContent);
      if (futureTenseMatch.Success) {
        var match = this._tdRegex.Match(tableContent, futureTenseMatch.Index);
        this.AddOrSet(resultSet, this._formMap["singular.future.first"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._formMap["singular.future.second"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._formMap["singular.future.third"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._formMap["plural.future.first"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._formMap["plural.future.second"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._formMap["plural.future.third"], this.GetWord(match.Groups["td"].Value));
      } else {
        var hasParticle = infinitive.EndsWith(" se");
        if (hasParticle) {
          var reorderedInfinitive = "se " + infinitive.Replace(" se", "");
          this.AddOrSet(resultSet, this._formMap["singular.future.first"], $"budu {reorderedInfinitive}");
          this.AddOrSet(resultSet, this._formMap["singular.future.second"], $"budeš {reorderedInfinitive}");
          this.AddOrSet(resultSet, this._formMap["singular.future.third"], $"bude {reorderedInfinitive}");
          this.AddOrSet(resultSet, this._formMap["plural.future.first"], $"budeme {reorderedInfinitive}");
          this.AddOrSet(resultSet, this._formMap["plural.future.second"], $"budete {reorderedInfinitive}");
          this.AddOrSet(resultSet, this._formMap["plural.future.third"], $"budou {reorderedInfinitive}");
        } else {
          this.AddOrSet(resultSet, this._formMap["singular.future.first"], $"budu {infinitive}");
          this.AddOrSet(resultSet, this._formMap["singular.future.second"], $"budeš {infinitive}");
          this.AddOrSet(resultSet, this._formMap["singular.future.third"], $"bude {infinitive}");
          this.AddOrSet(resultSet, this._formMap["plural.future.first"], $"budeme {infinitive}");
          this.AddOrSet(resultSet, this._formMap["plural.future.second"], $"budete {infinitive}");
          this.AddOrSet(resultSet, this._formMap["plural.future.third"], $"budou {infinitive}");
        }
      }
    }
  }
}
