﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.Text;

namespace wikiscraper {
  public class Program {
    public static void Main(string[] args) {
      Program.DoDownloadingAndParsing();
      // Program.DoParsingFromSampleFileVerb();
      // Program.DoParsingFromSampleFileNoun();
      // Program.DoParsingFromSampleFileAdjective();
      // Program.DoDirectoryScanAndParsing();
    }

    private static void DoParsingFromSampleFileVerb() {
      var verbSampleContent = File.ReadAllText(@"..\samples\sample-verb-3.html");
      var parser = new VerbParser();
      var (verb, resultSet) = parser.Parse(verbSampleContent);
      foreach (var res in resultSet) {
        Console.WriteLine("{0}: {1}", res.Key, res.Value);
      }

      var generator = new VerbGenerator();
      var verbDef = generator.Generate(verb, resultSet);
      Console.WriteLine(verbDef);
    }

    private static void DoParsingFromSampleFileNoun() {
      var nounSampleContent = File.ReadAllText(@"..\samples\sample-noun-0.html");
      var parser = new NounParser();
      var (verb, resultSet) = parser.Parse(nounSampleContent);
      foreach (var res in resultSet) {
        Console.WriteLine("{0}: {1}", res.Key, res.Value);
      }

      var generator = new NounGenerator();
      var verbDef = generator.Generate(verb, resultSet);
      Console.WriteLine(verbDef);
    }

    private static void DoParsingFromSampleFileAdjective() {
      var adjectiveSampleContent = File.ReadAllText(@"..\samples\sample-adjective-0.html");
      var parser = new AdjectiveParser();
      var (verb, resultSet) = parser.Parse(adjectiveSampleContent);
      foreach (var res in resultSet) {
        Console.WriteLine("{0}: {1}", res.Key, res.Value);
      }

      var generator = new AdjectiveGenerator();
      var verbDef = generator.Generate(verb, resultSet);
      Console.WriteLine(verbDef);
    }

    public static void DoDownloadingAndParsing() {
      Program.DoDownloadingAndParsingVerbs();
      Program.DoDownloadingAndParsingNouns();
      Program.DoDownloadingAndParsingAdjectives();
    }

    public static void DoDownloadingAndParsingVerbs() {
      var parser = new VerbParser();
      var generator = new VerbGenerator();
      var walker = new VerbWalker();
      var verbClassBuilder = new StringBuilder(3500000);
      var configFunctionBuilder = new StringBuilder(100000);
      var wordList = new List<string>();
      Program.ConfigFunctionHeader(configFunctionBuilder);
      walker.ItemReady += (o, e) => {
        Program.ParseAndGenerateFromFile(e.FilePath, parser, generator, verbClassBuilder, configFunctionBuilder, wordList);
      };

      const string startUrl = @"https://cs.wiktionary.org/wiki/Kategorie:%C4%8Cesk%C3%A1_slovesa";
      var verbFiles = walker.DownloadAllPages(startUrl);
      Program.ConfigFunctionFooter(configFunctionBuilder);
      Program.SaveFile(configFunctionBuilder, verbClassBuilder, "verbs");
    }

    public static void DoDownloadingAndParsingNouns() {
      var parser = new NounParser();
      var generator = new NounGenerator();
      var walker = new NounWalker();
      var classBuilder = new StringBuilder(3500000);
      var configFunctionBuilder = new StringBuilder(100000);
      var wordList = new List<string>();
      Program.ConfigFunctionHeader(configFunctionBuilder);
      walker.ItemReady += (o, e) => {
        Program.ParseAndGenerateFromFile(e.FilePath, parser, generator, classBuilder, configFunctionBuilder, wordList);
      };

      const string startUrl = @"https://cs.wiktionary.org/w/index.php?title=Kategorie:%C4%8Cesk%C3%A1_substantiva";
      var nounsFiles = walker.DownloadAllPages(startUrl);
      Program.ConfigFunctionFooter(configFunctionBuilder);
      Program.SaveFile(configFunctionBuilder, classBuilder, "nouns");
    }

    public static void DoDownloadingAndParsingAdjectives() {
      var parser = new AdjectiveParser();
      var generator = new AdjectiveGenerator();
      var walker = new AdjectiveWalker();
      var classBuilder = new StringBuilder(3500000);
      var configFunctionBuilder = new StringBuilder(100000);
      var wordList = new List<string>();
      Program.ConfigFunctionHeader(configFunctionBuilder);
      walker.ItemReady += (o, e) => {
        Program.ParseAndGenerateFromFile(e.FilePath, parser, generator, classBuilder, configFunctionBuilder, wordList);
      };

      const string startUrl = @"https://cs.wiktionary.org/w/index.php?title=Kategorie:%C4%8Cesk%C3%A1_adjektiva";
      var adjectivesFiles = walker.DownloadAllPages(startUrl);
      Program.ConfigFunctionFooter(configFunctionBuilder);
      Program.SaveFile(configFunctionBuilder, classBuilder, "adjectives");
    }

    private static void ConfigFunctionHeader(StringBuilder configFunctionBuilder) {
      configFunctionBuilder.AppendLine("export function configure(aurelia: FrameworkConfiguration) {");
    }

    private static void ConfigFunctionFooter(StringBuilder configFunctionBuilder) {
      configFunctionBuilder.AppendLine("}");
    }

    public static void DoDirectoryScanAndParsing() {
      Program.DoDirectoryScanAndParsingVerbs();
      Program.DoDirectoryScanAndParsingNouns();
      Program.DoDirectoryScanAndParsingAdjectives();
    }

    public static void DoDirectoryScanAndParsingVerbs() {
      var parser = new VerbParser();
      var generator = new VerbGenerator();
      var classBuilder = new StringBuilder(3500000);
      var configFunctionBuilder = new StringBuilder(100000);
      var wordList = new List<string>();
      Program.ConfigFunctionHeader(configFunctionBuilder);
      foreach (var file in Directory.EnumerateFiles("d:\\temp\\wikiscraper\\verbs", "*.html")) {
        Program.ParseAndGenerateFromFile(file, parser, generator, classBuilder, configFunctionBuilder, wordList);
      }
      Program.ConfigFunctionFooter(configFunctionBuilder);
      Program.SaveFile(configFunctionBuilder, classBuilder, "verbs");
    }

    public static void DoDirectoryScanAndParsingNouns() {
      var parser = new NounParser();
      var generator = new NounGenerator();
      var classBuilder = new StringBuilder(3500000);
      var configFunctionBuilder = new StringBuilder(100000);
      var wordList = new List<string>();
      Program.ConfigFunctionHeader(configFunctionBuilder);
      foreach (var file in Directory.EnumerateFiles("d:\\temp\\wikiscraper\\nouns", "*.html")) {
        Program.ParseAndGenerateFromFile(file, parser, generator, classBuilder, configFunctionBuilder, wordList);
      }
      Program.ConfigFunctionFooter(configFunctionBuilder);
      Program.SaveFile(configFunctionBuilder, classBuilder, "nouns");
    }

    public static void DoDirectoryScanAndParsingAdjectives() {
      var parser = new AdjectiveParser();
      var generator = new AdjectiveGenerator();
      var classBuilder = new StringBuilder(3500000);
      var configFunctionBuilder = new StringBuilder(100000);
      var wordList = new List<string>();
      Program.ConfigFunctionHeader(configFunctionBuilder);
      foreach (var file in Directory.EnumerateFiles("d:\\temp\\wikiscraper\\adjectives", "*.html")) {
        Program.ParseAndGenerateFromFile(file, parser, generator, classBuilder, configFunctionBuilder, wordList);
      }
      Program.ConfigFunctionFooter(configFunctionBuilder);
      Program.SaveFile(configFunctionBuilder, classBuilder, "adjectives");
    }

    private static void ParseAndGenerateFromFile(string filePath, Parser parser, Generator generator, StringBuilder classBuilder, StringBuilder configFunctionBuilder, List<string> wordList) {
      try {
        Console.WriteLine("Parsing and generating file {0}", filePath);
        var verbSampleContent = File.ReadAllText(filePath);
        var (word, resultSet) = parser.Parse(verbSampleContent);
        if (!string.IsNullOrEmpty(word) && !wordList.Contains(word.ToLowerInvariant())) {
          wordList.Add(word.ToLowerInvariant());
          classBuilder.AppendLine($"// Source file: {Path.GetFileName(filePath)}");
          classBuilder.Append(generator.Generate(word, resultSet));
          configFunctionBuilder.AppendLine($"  aurelia.singleton(lang.LanguageStructure, {generator.MakeClassName(word)});");
        }
      } catch {
        Console.WriteLine("Failed to parse or generate file {0}", filePath);
        classBuilder.Append($"\n\n//\n// !!! PARSING ERROR in file {Path.GetFileName(filePath)} !!!\n//\n\n");
      }
    }

    private static void SaveFile(StringBuilder configFunctionBuilder, StringBuilder fileBuilder, string type) {
      var file = $"d:\\temp\\wikiscraper\\{type}-wiki.ts";
      if (File.Exists(file)) {
        Console.WriteLine($"{file} already exists, deleting...");
        File.Delete(file);
      }

      using (var verbFile = File.CreateText(file)) {
        Console.WriteLine($"Writing {file}...");
        verbFile.WriteLine(@"import {FrameworkConfiguration} from 'aurelia-framework';");
        verbFile.WriteLine(@"import * as lang from 'snowatch/language-structures';");
        verbFile.WriteLine();
        verbFile.WriteLine(@"// generated from wikipedia list of czech {type}");
        verbFile.WriteLine(@"// extraction tool repo: https://bitbucket.org/jIRI/wikiscraper");
        verbFile.WriteLine();
        verbFile.WriteLine();
        verbFile.Write(configFunctionBuilder.ToString());
        verbFile.WriteLine();
        verbFile.WriteLine("//----------------------------------------------------------------------------------------");
        verbFile.WriteLine();
        verbFile.Write(fileBuilder.ToString());
      }
      Console.WriteLine($"Writing {file} finished.");
    }
  }
}
