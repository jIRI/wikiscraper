﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace wikiscraper {
  public class NounParser : Parser {
    private Regex _nounRegex = new Regex(@"\<h1 id\=""firstHeading"" class\=""firstHeading"" lang\=""cs""\>(?<noun>.*)\<\/h1\>");
    private Regex _tableStartRegex = new Regex(@"\<table\s+class=\""deklinace substantivum\""\>");
    private Regex _tableEndRegex = new Regex(@"\<\/table\>");
    private Regex _typeMasculineAnimate = new Regex(@"<i>rod mužský životný</i>");
    private Regex _typeMasculineInanimate = new Regex(@"<i>rod mužský neživotný</i>");
    private Regex _typeFeminine = new Regex(@"<i>rod ženský</i>");
    private Regex _typeNeutral = new Regex(@"<i>rod střední</i>");
    private Regex _trRegex = new Regex(@"\<tr\>");
    private Regex _tdRegex = new Regex(@"\<td\>(?<td>.*)\s*\<\/td\>");
    private Dictionary<string, string[]> _nounMap = new Dictionary<string, string[]> {
      ["singular.gender"] = new string[] { "singular", "gender" },
      ["singular.genderSubtype"] = new string[] { "singular", "genderSubtype" },
      ["singular.number"] = new string[] { "singular", "number" },
      ["singular.nominative"] = new string[] { "singular", "nominative" },
      ["singular.genitive"] = new string[] { "singular", "genitive" },
      ["singular.dative"] = new string[] { "singular", "dative" },
      ["singular.accusative"] = new string[] { "singular", "accusative" },
      ["singular.vocative"] = new string[] { "singular", "vocative" },
      ["singular.locative"] = new string[] { "singular", "locative" },
      ["singular.instrumental"] = new string[] { "singular", "instrumental" },

      ["plural.gender"] = new string[] { "plural", "gender" },
      ["plural.genderSubtype"] = new string[] { "plural", "genderSubtype" },
      ["plural.number"] = new string[] { "plural", "number" },
      ["plural.nominative"] = new string[] { "plural", "nominative" },
      ["plural.genitive"] = new string[] { "plural", "genitive" },
      ["plural.dative"] = new string[] { "plural", "dative" },
      ["plural.accusative"] = new string[] { "plural", "accusative" },
      ["plural.vocative"] = new string[] { "plural", "vocative" },
      ["plural.locative"] = new string[] { "plural", "locative" },
      ["plural.instrumental"] = new string[] { "plural", "instrumental" },
    };

    public override (string word, Dictionary<string[], string> resultSet) Parse(string pageContent) {
      var resultSet = new Dictionary<string[], string>();
      var nounMatch = this._nounRegex.Match(pageContent);
      string noun = nounMatch.Groups["noun"].Value;
      var tableStartMatches = this._tableStartRegex.Matches(pageContent);
      foreach (Match match in tableStartMatches) {
        int startPos = match.Index + match.Length;
        var tableEndMatch = this._tableEndRegex.Match(pageContent, startPos);
        var preTableContent = pageContent.Substring(nounMatch.Index, match.Index - nounMatch.Index);
        var tableContent = pageContent.Substring(startPos, tableEndMatch.Index - startPos);

        if (!string.IsNullOrWhiteSpace(tableContent)) {
          this.ParseType(preTableContent, resultSet, "singular");
          this.ParseType(preTableContent, resultSet, "plural");
          this.ParseNoun(tableContent, resultSet);
        }
        break;
      }

      if(resultSet.Count == 0 || resultSet.Values.Any(v => string.IsNullOrEmpty(v)) ) {
        return (null, null);
      }

      return (noun, resultSet);
    }

    private void ParseType(string tableContent, Dictionary<string[], string> resultSet, string number) {
      var typeMatch = this._typeMasculineAnimate.Match(tableContent);
      if (typeMatch.Success) {
        this.AddOrSet(resultSet, this._nounMap[$"{number}.gender"], "lang.Gender.masculine");
        this.AddOrSet(resultSet, this._nounMap[$"{number}.genderSubtype"], "lang.GenderSubtype.animate");
        this.AddOrSet(resultSet, this._nounMap[$"{number}.number"], $"lang.Number.{number}");
        return;
      }

      typeMatch = this._typeMasculineInanimate.Match(tableContent);
      if (typeMatch.Success) {
        this.AddOrSet(resultSet, this._nounMap[$"{number}.gender"], "lang.Gender.masculine");
        this.AddOrSet(resultSet, this._nounMap[$"{number}.genderSubtype"], "lang.GenderSubtype.inanimate");
        this.AddOrSet(resultSet, this._nounMap[$"{number}.number"], $"lang.Number.{number}");
        return;
      }

      typeMatch = this._typeFeminine.Match(tableContent);
      if (typeMatch.Success) {
        this.AddOrSet(resultSet, this._nounMap[$"{number}.gender"], "lang.Gender.feminine");
        this.AddOrSet(resultSet, this._nounMap[$"{number}.genderSubtype"], "lang.GenderSubtype.animate");
        this.AddOrSet(resultSet, this._nounMap[$"{number}.number"], $"lang.Number.{number}");
        return;
      }

      typeMatch = this._typeNeutral.Match(tableContent);
      if (typeMatch.Success) {
        this.AddOrSet(resultSet, this._nounMap[$"{number}.gender"], "lang.Gender.neutral");
        this.AddOrSet(resultSet, this._nounMap[$"{number}.genderSubtype"], "lang.GenderSubtype.animate");
        this.AddOrSet(resultSet, this._nounMap[$"{number}.number"], $"lang.Number.{number}");
        return;
      }
    }

    private void ParseNoun(string tableContent, Dictionary<string[], string> resultSet) {
      var caseMatch = this._trRegex.Match(tableContent);
      var offset = 0;
      if (caseMatch.Success) {
        var match = this._tdRegex.Match(tableContent, caseMatch.Index);
        this.AddOrSet(resultSet, this._nounMap["singular.nominative"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._nounMap["plural.nominative"], this.GetWord(match.Groups["td"].Value));
        offset = match.Index;
      }

      caseMatch = this._trRegex.Match(tableContent, offset);
      if (caseMatch.Success) {
        var match = this._tdRegex.Match(tableContent, caseMatch.Index);
        this.AddOrSet(resultSet, this._nounMap["singular.genitive"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._nounMap["plural.genitive"], this.GetWord(match.Groups["td"].Value));
        offset = match.Index;
      }

      caseMatch = this._trRegex.Match(tableContent, offset);
      if (caseMatch.Success) {
        var match = this._tdRegex.Match(tableContent, caseMatch.Index);
        this.AddOrSet(resultSet, this._nounMap["singular.dative"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._nounMap["plural.dative"], this.GetWord(match.Groups["td"].Value));
        offset = match.Index;
      }

      caseMatch = this._trRegex.Match(tableContent, offset);
      if (caseMatch.Success) {
        var match = this._tdRegex.Match(tableContent, caseMatch.Index);
        this.AddOrSet(resultSet, this._nounMap["singular.accusative"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._nounMap["plural.accusative"], this.GetWord(match.Groups["td"].Value));
        offset = match.Index;
      }

      caseMatch = this._trRegex.Match(tableContent, offset);
      if (caseMatch.Success) {
        var match = this._tdRegex.Match(tableContent, caseMatch.Index);
        this.AddOrSet(resultSet, this._nounMap["singular.vocative"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._nounMap["plural.vocative"], this.GetWord(match.Groups["td"].Value));
        offset = match.Index;
      }

      caseMatch = this._trRegex.Match(tableContent, offset);
      if (caseMatch.Success) {
        var match = this._tdRegex.Match(tableContent, caseMatch.Index);
        this.AddOrSet(resultSet, this._nounMap["singular.locative"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._nounMap["plural.locative"], this.GetWord(match.Groups["td"].Value));
        offset = match.Index;
      }

      caseMatch = this._trRegex.Match(tableContent, offset);
      if (caseMatch.Success) {
        var match = this._tdRegex.Match(tableContent, caseMatch.Index);
        this.AddOrSet(resultSet, this._nounMap["singular.instrumental"], this.GetWord(match.Groups["td"].Value));
        match = match.NextMatch();
        this.AddOrSet(resultSet, this._nounMap["plural.instrumental"], this.GetWord(match.Groups["td"].Value));
        offset = match.Index;
      }
    }
  }
}
