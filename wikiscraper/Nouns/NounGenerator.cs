﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wikiscraper {
  public class NounGenerator : Generator {
    public override string Generate(string noun, Dictionary<string[], string> nounMap) {
      if (string.IsNullOrEmpty(noun)) {
        return null;
      }

      var numbers = from v in nounMap.Keys
                    group v by v[0] into vv
                    select vv;

      var needsPluralSuffix = nounMap[nounMap.Keys.Single(k => k[0] == "singular" && k[1] == "nominative")] == nounMap[nounMap.Keys.Single(k => k[0] == "plural" && k[1] == "nominative")];

      var nounDef = new StringBuilder(1024);
      foreach (var numberPair in numbers) {
        var isPlural = numberPair.Key == "plural";
        var nounName = nounMap[nounMap.Keys.Single(k => k[0] == numberPair.Key && k[1] == "nominative")].Trim().ToLowerInvariant();        
        var nounId = this.MakeId(nounName + (isPlural && needsPluralSuffix ? "-plural" : ""));
        var className = this.MakeClassName(nounName + (isPlural && needsPluralSuffix ? "Plural" : ""));
        nounDef.AppendFormat("export class {0} extends lang.Noun {{\n", className);
        nounDef.AppendFormat("  public static readonly ID = `${{lang.Noun.ID}}.{0}`;\n", nounId.Trim().ToLowerInvariant());
        nounDef.Append("  constructor() {\n");
        nounDef.Append("    super({\n");
        nounDef.AppendFormat("      noun: '{0}',\n", nounName);

        foreach (var nounKey in numberPair) {
          var key = nounKey[1];
          switch (key) {
            case var k when k == "gender" || k == "genderSubtype" || k == "number":
              nounDef.AppendFormat("      {0}: {1},\n", key, nounMap[nounKey].Trim());
              break;
            default:
              nounDef.AppendFormat("      {0}: '{1}',\n", key, nounMap[nounKey].Trim().ToLowerInvariant());
              break;
          }
        }

        nounDef.Append("    });\n");
        nounDef.Append("  }\n");
        nounDef.Append("}\n\n");
      }
      return nounDef.ToString();
    }
  }
}
